﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectOrienteret
{
    class Washer
    {
        private int powerUsage;
        private byte degree;
        private int rotation;
        private bool isOn;
        private int washTime;

        #region Properties
        public int PowerUsage
        {
            get
            {
                return powerUsage;
            }
            
            set
            {
                powerUsage = value;
            }
        }
        
        public byte Degree
        {
            get
            {
                return degree;
            }
            set
            {
                degree = value;
            }
        }
        
        public int Rotation
        {
            get
            {
                return rotation;
            }
            
            set
            {
                rotation = value;
            }

        }
        public int WashTime
        {
            get
            {
                return washTime;
            }
            set
            {
                washTime = value;
            }
        }
        #endregion

        //Powers on the washer, the other method will not work if this is not called first.
        public void Power()
        {
            this.isOn = true;
        }
        //Start the washing and sets the degree, washing time and rotation of the object to the parameters entered.
        public string StartWash(int time, int rotation, byte degree)
        {
            if (isOn == true)
            {
                
                this.degree = degree;
                this.washTime = time;
                this.rotation = rotation;
                return "Your wash has started with these settings\n Degree: " + degree + "\n washing time: " + washTime + "\n rotations: " + rotation;
            }
            else
            {
                return "System is not on";
            }
        }

        //A fun method to simulate washing time
        public void Washing()
        {
            if (isOn == true)
            {
                //Could use this.washTime instead of 5 to simulate real time
                for (int i = 0; i < 5; i++)
                {
                    Console.WriteLine("Washing..");
                    System.Threading.Thread.Sleep(1000);
                }
            }
            else
            {
                Console.WriteLine("System is not on");
            }
        }

        //Ends the wash and sets all values to 0 aswell as turning off the machine.
        public string EndWash()
        {
            if (isOn == true)
            {
                this.degree = 0;
                this.washTime = 0;
                this.rotation = 0;
                this.isOn = false;
                return "Your wash has now ended";
            }
            else
            {
                return "System is not on.";
            }
        }
    }
}
